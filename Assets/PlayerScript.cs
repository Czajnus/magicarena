﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerScript : NetworkBehaviour {
    // This script represents player connection to the server who might "own" other objects 
    // Like mages, spells etc etc
    // This script works for everyone and it's executing on every connected pc 

    // Use this for initialization
    public GameObject MagePrefab;
    GameObject myMage;

    void Start () {

        if (isServer == true)
        {
            SpawnMage();
        }

        if (!isLocalPlayer)
        {
            Destroy(myMage.GetComponent<Transform>().GetChild(1).gameObject);
            Destroy(myMage.GetComponent<Demo>());
        }

    }

	

	// Update is called once per frame
	void Update () {

        if (hasAuthority)
        {

        }
		
	}

	public void SpawnMage()
	{
        if (isServer == false)
        {
            Debug.LogError("SpawnTank: can do only server things");
            return;
        }

        // TODO: customazing mages. Different robes, staffs etc. etc.


        myMage = Instantiate(MagePrefab);
        

        NetworkServer.SpawnWithClientAuthority(myMage, connectionToClient);

        
	}
}
