﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MageScript : NetworkBehaviour {

    // Script will run on server and on all of the clients and mages

	// Use this for initialization
	void Start () {


		
	}

    float speed = 5;

    [SyncVar]
    Vector3 serverPosition;
    Vector3 serverPositionSmoothVelocity;
	
	// Update is called once per frame
	void Update () {

        if (isServer)
        {
            // Do some server specific thinks. Checking/maintenance/dealing dmg etc.
        }

        if(hasAuthority)
        {
            // This is my object and if i do something, network will listen
            AuthorityUpdate();
        }

        // Do generic updates for every client in the server (animations,movements and stuff
        // Are we in correct position?
        if (!hasAuthority)
        {
            // We don't "own" this object so we move object to server's position smoothly

            transform.position = Vector3.SmoothDamp(transform.position, serverPosition, ref serverPositionSmoothVelocity, .25f);

        }
		
	}

    void AuthorityUpdate()
    {
        float movementH = Input.GetAxis("Horizontal") *speed*Time.deltaTime;
        float movementV = Input.GetAxis("Vertical")*speed*Time.deltaTime;

        transform.Translate(movementH,0,movementV);

        CmdUpdatePosition(transform.position);

    }

    [Command]
    void CmdUpdatePosition(Vector3 newPosition)
    {
        //TODO: Check to be sure if this move is legal.

        serverPosition = newPosition;
    }
}
