﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeTrialScript : MonoBehaviour {

    public GameObject trialPrefab;
    public GameObject empty;
    private GameObject thisTrial;
    private GameObject empty2;
    private Vector3 startPos;
    private Plane objPlane;
    private bool createPanel;
    private GameObject newCanvas;
    private GameObject panel;

    private Texture2D tex;
    private float width;
    private float height;
    private float startX;
    private float startY;
    private Rect rex;
    private bool sscreen;

    public RectTransform rectT;
    private Rect rect;


    void Start () {

        objPlane = new Plane(Camera.main.transform.forward*-1 , this.transform.position);
        createPanel = false;
		
	}
	
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {

            thisTrial = (GameObject)Instantiate(trialPrefab, trialPrefab.transform.position, Quaternion.identity);
            print("this: " + this.transform.position);
            print("trial: " + thisTrial.transform.position);
            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDistance;
            if (objPlane.Raycast(mRay, out rayDistance))
                startPos = mRay.GetPoint(rayDistance);

            if (createPanel == false)
            {

                newCanvas = new GameObject("Canvas");
                Canvas c = newCanvas.AddComponent<Canvas>();
                c.renderMode = RenderMode.WorldSpace;
                c.transform.position = new Vector3(0, 0, 10);
                newCanvas.AddComponent<CanvasScaler>();
                newCanvas.AddComponent<GraphicRaycaster>();
                panel = new GameObject("Panel");
                panel.AddComponent<CanvasGroup>();
                panel.GetComponent<CanvasGroup>().alpha = .7f;
                panel.AddComponent<CanvasRenderer>();
                Image i = panel.AddComponent<Image>();
                i.color = Color.grey;
                panel.transform.SetParent(newCanvas.transform, false);
                panel.GetComponent<RectTransform>().sizeDelta = new Vector2(15, 15);
                createPanel = true;
            }

          

        }
        else if (Input.GetMouseButton(0))
        {

            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDistance;
            if (objPlane.Raycast(mRay, out rayDistance))
                thisTrial.transform.position = mRay.GetPoint(rayDistance);
        }


        
        else if (Input.GetMouseButtonUp(0))
        {
           // if (Vector3.Distance(thisTrial.transform.position, startPos) < 0.1)
            //    Destroy(thisTrial);

          //  Destroy(newCanvas);
           // createPanel = false;

        }

          if (Input.GetKeyDown(KeyCode.Space))
            {
            width = 500;
            height = 200;
            startX = 50;
            startY = 50;


            print("width: " + Screen.width);
            print("width: " + Screen.width*.5f);
            print("hei: " + Screen.height);

            tex = new Texture2D(500, 500, TextureFormat.RGB24, false);

            rex = new Rect((Screen.width-width)/2,(Screen.height-height)/2,width,height);
            //rex.center = new Vector2(100, 100);

                tex.ReadPixels(rex, 0, 0);
                tex.Apply();

                // Encode texture into PNG
                var bytes = tex.EncodeToPNG();
                Destroy(tex);

                System.IO.File.WriteAllBytes("D:/Unity/NowyFolder/SavedScreen.png", bytes);
                sscreen = false;
            }

    }
}
