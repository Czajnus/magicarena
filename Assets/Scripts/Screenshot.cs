﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshot : MonoBehaviour {

    private Texture2D tex;
    private int width;
    private int height;
    private float startX;
    private float startY;
    private Rect rex;
    private bool sscreen;

    // Use this for initialization
    void Start () {
        sscreen = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            width = 400;
            height = 400;
            startX =100;
            startY = 100;
            tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            rex = new Rect(startX, startY, width, height);

            tex.ReadPixels(rex, 0, 0);
            tex.Apply();

            // Encode texture into PNG
            var bytes = tex.EncodeToPNG();
            Destroy(tex);

            System.IO.File.WriteAllBytes("D:/Unity/NowyFolder/SavedScreen.png", bytes);
            sscreen = false;
        }

    }
    private void OnPostRender()
    {
        if (sscreen == true)
        {
            width = 300;
            height = 300;
            startX = 200;
            startY = 100;
            tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            rex = new Rect(startX, startY, width, height);

            tex.ReadPixels(rex, 0, 0);
            tex.Apply();

            // Encode texture into PNG
            var bytes = tex.EncodeToPNG();
            Destroy(tex);

            System.IO.File.WriteAllBytes("D:/Unity/NowyFolder/SavedScreen.png", bytes);
            sscreen = false;
        }
    }
}
